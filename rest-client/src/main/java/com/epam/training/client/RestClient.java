package com.epam.training.client;
import com.epam.training.constants.Constants;
import com.epam.training.entity.Teacher;
import com.epam.training.exceptions.NotFoundException;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import org.apache.cxf.jaxrs.client.WebClient;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

public class RestClient implements Constants{

    private List<Object> providers;

    public RestClient() {
        this.providers = new ArrayList<>();
        providers.add(new JacksonJaxbJsonProvider());
    }

    public List<Object> getProviders() {
        return providers;
    }

    public void setProviders(List<Object> providers) {
        this.providers = providers;
    }

    public boolean addTeacher(Teacher teacher) {
        WebClient client = WebClient.create(REST_URI, providers);
        client.path(ADD_TEACHER_PATH);
        client = client.accept(MediaType.APPLICATION_FORM_URLENCODED).type(MediaType.APPLICATION_JSON);
        Response response = client.post(teacher);

        if (Response.Status.fromStatusCode(response.getStatus()).
                equals(Response.Status.NOT_MODIFIED)) {
            return false;
        }
        return true;
    }

    public Teacher getTeacher(int id) throws NotFoundException {
        WebClient client = WebClient.create(REST_URI, providers);
        client.path(GET_TEACHER_PATH);
        client.path(ID_PARAM, id);
        client = client.accept(MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_FORM_URLENCODED);

        Response response = client.get();
        Teacher teacherResult;
        if (Response.Status.fromStatusCode(response.getStatus()).
                equals(Response.Status.NOT_FOUND)) {
            throw new NotFoundException();
        } else {
            teacherResult = response.readEntity(Teacher.class);
        }
        return teacherResult;
    }

    public Teacher getXmlTeacherByJson(Teacher teacher) throws NotFoundException {
        WebClient client = WebClient.create(REST_URI, providers);
        client.path(GET_XML_TEACHER_BY_JSON_PATH);
        client = client.accept(MediaType.APPLICATION_XML).type(MediaType.APPLICATION_JSON);
        Response response = client.post(teacher);
        Teacher teacherResult;
        if (Response.Status.fromStatusCode(response.getStatus()).
                equals(Response.Status.NOT_FOUND)) {
            throw new NotFoundException();
        } else {
            teacherResult = response.readEntity(Teacher.class);
        }
        return teacherResult;
    }

    public Teacher getBusiestTeacher() throws NotFoundException {
        WebClient client = WebClient.create(REST_URI, providers);
        client.path(GET_BUSIEST_TEACHER_PATH);
        client = client.accept(MediaType.APPLICATION_XML).type(MediaType.APPLICATION_FORM_URLENCODED);

        Response response = client.get();
        Teacher teacherResult;
        if (Response.Status.fromStatusCode(response.getStatus()).
                equals(Response.Status.NOT_FOUND)) {
            throw new NotFoundException();
        } else {
            teacherResult = response.readEntity(Teacher.class);
        }
        return teacherResult;
    }


    public boolean updateTeacher(Teacher teacher) {
        WebClient client = WebClient.create(REST_URI, providers);
        client.path(UPDATE_TEACHER_PATH);
        client = client.accept(MediaType.APPLICATION_FORM_URLENCODED).type(MediaType.APPLICATION_JSON);
        Response response = client.put(teacher);

        if (Response.Status.fromStatusCode(response.getStatus()).
                equals(Response.Status.NOT_MODIFIED)) {
            return false;
        }
        return true;
    }

    public boolean deleteTeacher(int id) {
        WebClient client = WebClient.create(REST_URI, providers);
        client.path(DELETE_TEACHER_PATH);
        client.path(ID_PARAM, id);
        client = client.accept(MediaType.APPLICATION_FORM_URLENCODED).type(MediaType.APPLICATION_FORM_URLENCODED);
        Response response = client.delete();

        if (Response.Status.fromStatusCode(response.getStatus()).
                equals(Response.Status.NOT_MODIFIED)) {
            return false;
        }
        return true;
    }

}

package com.epam.training;

import com.epam.training.client.RestClient;
import com.epam.training.entity.Lesson;
import com.epam.training.entity.Teacher;
import com.epam.training.exceptions.NotFoundException;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Runner {
    public static void main(String[] args) {
        RestClient client = new RestClient();
        int firstTeacherId = 1;
        int secondTeacherId = 2;

        //Add teacher
        Teacher teacherForAdding = new Teacher();
        teacherForAdding.setId(firstTeacherId);
        teacherForAdding.setName("first");
        GregorianCalendar c = new GregorianCalendar();
        c.setTime(new Date());
        XMLGregorianCalendar gregorianCalendar;
        try {
            gregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
            teacherForAdding.setBirthday(gregorianCalendar);
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
        teacherForAdding.getLesson().add(new Lesson());
        boolean addTeacherResult = client.addTeacher(teacherForAdding);

        if (addTeacherResult) {
            System.out.println("Teacher added");
        } else {
            System.out.println("Teacher not added");
        }




        try {
            //Get teacher
            Teacher getTeacher = client.getTeacher(firstTeacherId);
            System.out.println("\n\nTeacher get: " + getTeacher);

            //Get xml teacher from json
            Teacher xmlTeacherFromJson = client.getXmlTeacherByJson(getTeacher);
            System.out.println("Json source:" + getTeacher);
            System.out.println("Xml result:" + xmlTeacherFromJson);

            //Get busiest teacher
            Teacher secondTeacher = client.getTeacher(firstTeacherId);
            secondTeacher.setId(secondTeacherId);

            Lesson lesson = new Lesson();
            lesson.setId(1);
            lesson.setName("math");
            lesson.setDuration(45);

            secondTeacher.getLesson().add(lesson);
            boolean addSecondTeacherResult = client.addTeacher(secondTeacher);

            if(addSecondTeacherResult){
                Teacher busiestTeacher = client.getBusiestTeacher();
                System.out.println("Busiest Teacher" + busiestTeacher);
            } else {
                System.out.println("Cant access busiest teacher");
            }

            //Update teacher
            Teacher teacherBefore = client.getTeacher(firstTeacherId);
            System.out.println("Teacher before update: " + teacherBefore);

            teacherBefore.setName("UpdatedTeacher");
            boolean updateResult = client.updateTeacher(teacherBefore);

            if (updateResult) {
                Teacher teacherAfter = client.getTeacher(firstTeacherId);
                System.out.println("Teacher after update: " + teacherAfter);
            } else {
                System.out.println("Teacher: " + teacherBefore + " not updated");
            }

            //Delete teacher
            boolean deletingResult = client.deleteTeacher(firstTeacherId);
            System.out.print("Teacher with id: " + firstTeacherId);
            if (deletingResult) {
                System.out.println(" deleted");
            } else {
                System.out.println(" not deleted");
            }

            try {
                client.getTeacher(firstTeacherId);
            } catch (NotFoundException e){
                e.printStackTrace();
                System.out.println("Teacher with id:" + firstTeacherId + " was deleted");
            }

        } catch (NotFoundException e) {
            e.printStackTrace();
            System.out.println("Resource not found");
        }
        System.out.println("\n\n");
    }
}

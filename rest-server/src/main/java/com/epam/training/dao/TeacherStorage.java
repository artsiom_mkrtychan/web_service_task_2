package com.epam.training.dao;

import com.epam.training.entity.Lesson;
import com.epam.training.entity.Teacher;

import java.util.HashMap;
import java.util.Map;

public class TeacherStorage {

    private static volatile TeacherStorage instance;
    private Map<Integer, Teacher> teachers;

    private TeacherStorage() {
        teachers = new HashMap<>();
    }

    public static TeacherStorage getInstance() {
        TeacherStorage localInstance = instance;
        if (localInstance == null) {
            synchronized (TeacherStorage.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new TeacherStorage();
                }
            }
        }
        return localInstance;
    }

    public void addTeacher(Teacher teacher) {
        teachers.put(teacher.getId(), teacher);
    }

    public Teacher getTeacher(int teacherId) {
        return teachers.get(teacherId);
    }

    public boolean updateTeacher(Teacher teacher) {
        Teacher old = teachers.get(teacher.getId());
        teachers.put(teacher.getId(), teacher);

        return !old.equals(teacher);
    }

    public boolean deleteTeacher(int teacherId) {
        Teacher removedTeacher = teachers.remove(teacherId);
        return removedTeacher != null;
    }

    public Teacher getBusiestTeacher() {
        int busiestTeacherId = 0;
        int busiestTeacherLessonsDuration = 0;

        for (Map.Entry<Integer, Teacher> teacherEntry : teachers.entrySet()) {
            Teacher tempTeacher = teacherEntry.getValue();
            int tempLessonDuration = 0;

            for (Lesson lesson : tempTeacher.getLesson()) {
                tempLessonDuration += lesson.getDuration();
            }

            if (tempLessonDuration > busiestTeacherLessonsDuration) {
                busiestTeacherId = tempTeacher.getId();
                busiestTeacherLessonsDuration = tempLessonDuration;
            }
        }

        return teachers.get(busiestTeacherId);
    }


}

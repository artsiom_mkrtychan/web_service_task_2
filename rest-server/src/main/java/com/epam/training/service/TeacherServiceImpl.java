package com.epam.training.service;

import com.epam.training.entity.Teacher;
import com.epam.training.dao.TeacherStorage;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;

@Service("teacherService")
public class TeacherServiceImpl implements ITeacherService {
    private TeacherStorage storage = TeacherStorage.getInstance();

    @Override
    public Response addTeacher(Teacher teacher) {
        Response response = null;

        if (teacher != null) {
            storage.addTeacher(teacher);
            response = Response.status(Response.Status.CREATED).build();
        } else {
            response = Response.status(Response.Status.NOT_MODIFIED).build();
        }
        return response;
    }

    @Override
    public Response getTeacher(int teacherId) {
        Teacher teacher = storage.getTeacher(teacherId);
        Response response = null;
        if (teacher == null) {
            response = Response.status(Response.Status.NOT_FOUND).build();
        } else {
            response = Response.ok(teacher).build();
        }

        return response;
    }

    @Override
    public Response getXmlTeacherByJson(Teacher teacher) {
        Response response = null;
        if (teacher == null) {
            response = Response.status(Response.Status.NOT_FOUND).build();
        } else {
            response = Response.ok(teacher).build();
        }
        return response;
    }

    @Override
    public Response getBusiestTeacher() {
        Teacher teacher = storage.getBusiestTeacher();
        Response response = null;
        if (teacher == null) {
            response = Response.status(Response.Status.NOT_FOUND).build();
        } else {
            response = Response.ok(teacher).build();
        }
        return response;
    }

    @Override
    public Response updateTeacher(Teacher teacher) {
        boolean result = storage.updateTeacher(teacher);
        Response response = null;
        if (!result) {
            response = Response.status(Response.Status.NOT_MODIFIED).build();
        } else {
            response = Response.status(Response.Status.OK).build();
        }
        return response;
    }

    @Override
    public Response deleteTeacher(int teacherId) {
        boolean result = storage.deleteTeacher(teacherId);
        Response response = null;
        if (!result) {
            response = Response.status(Response.Status.NOT_MODIFIED).build();
        } else {
            response = Response.status(Response.Status.OK).build();
        }
        return response;
    }
}

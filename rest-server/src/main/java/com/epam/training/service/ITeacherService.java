package com.epam.training.service;

import com.epam.training.entity.Teacher;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/teacherservice")
public interface ITeacherService {
    // Basic CRUD operations for Player Service

    // http://localhost:8080/ApacheCXF-XML-JSON-IO/services/playerservice/addplayer
    @POST
    @Path("addteacher")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_FORM_URLENCODED})
    public Response addTeacher(Teacher teacher);

    // http://localhost:8080/ApacheCXF-XML-JSON-IO/services/playerservice/getplayer/238
    @GET
    @Path("getteacher/{id}")
    @Consumes({MediaType.APPLICATION_FORM_URLENCODED})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response getTeacher(@PathParam("id") int teacherId);

    //http://localhost:8080/rest-server/services/teacherservice/getxmlteacherbyjson
    @POST
    @Path("getxmlteacherbyjson")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML})
    public Response getXmlTeacherByJson(Teacher teacher);

    // http://localhost:8080/ApacheCXF-XML-JSON-IO/services/playerservice/getplayer/238
    @GET
    @Path("getbusiestteacher")
    @Consumes({MediaType.APPLICATION_FORM_URLENCODED})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response getBusiestTeacher();

    // http://localhost:8080/ApacheCXF-XML-JSON-IO/services/playerservice/updateplayer
    @PUT
    @Path("updateteacher")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_FORM_URLENCODED})
    public Response updateTeacher(Teacher teacher);

    // http://localhost:8080/ApacheCXF-XML-JSON-IO/services/playerservice/deleteplayer
    @DELETE
    @Path("deleteteacher/{id}")
    @Consumes({MediaType.APPLICATION_FORM_URLENCODED})
    @Produces({MediaType.APPLICATION_FORM_URLENCODED})
    public Response deleteTeacher(@PathParam("id") int teacherId);



}

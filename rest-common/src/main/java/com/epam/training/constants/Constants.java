package com.epam.training.constants;

public interface Constants {
    String REST_URI = "http://localhost:8080/rest-server/services/teacherservice";
    String ADD_TEACHER_PATH = "addteacher";
    String GET_TEACHER_PATH = "getteacher";
    String GET_XML_TEACHER_BY_JSON_PATH = "getxmlteacherbyjson";
    String GET_BUSIEST_TEACHER_PATH = "getbusiestteacher";
    String UPDATE_TEACHER_PATH = "updateteacher";
    String DELETE_TEACHER_PATH = "deleteteacher";
    String ID_PARAM = "{id}";
}

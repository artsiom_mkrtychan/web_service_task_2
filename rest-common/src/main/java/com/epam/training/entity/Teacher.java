package com.epam.training.entity;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "id",
        "name",
        "birthday",
        "lesson"
})
@XmlRootElement(name = "Teacher")
public class Teacher {

    protected int id;
    protected String name;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar birthday;
    @XmlElement(name = "lesson")
    protected List<Lesson> lesson;


    public Teacher() {
    }

    public int getId() {
        return id;
    }


    public void setId(int value) {
        this.id = value;
    }


    public String getName() {
        return name;
    }


    public void setName(String value) {
        this.name = value;
    }

    public XMLGregorianCalendar getBirthday() {
        return birthday;
    }


    public void setBirthday(XMLGregorianCalendar value) {
        this.birthday = value;
    }


    public List<Lesson> getLesson() {
        if (lesson == null) {
            lesson = new ArrayList<Lesson>();
        }
        return this.lesson;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Teacher teacher = (Teacher) o;

        if (id != teacher.id) return false;
        if (name != null ? !name.equals(teacher.name) : teacher.name != null) return false;
        if (birthday != null ? !birthday.equals(teacher.birthday) : teacher.birthday != null) return false;
        return lesson != null ? lesson.equals(teacher.lesson) : teacher.lesson == null;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (birthday != null ? birthday.hashCode() : 0);
        result = 31 * result + (lesson != null ? lesson.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", birthday=" + birthday +
                ", lesson=" + lesson +
                '}';
    }
}

